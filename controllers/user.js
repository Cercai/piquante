const argon2 = require('argon2');
const jwt = require('jsonwebtoken');

const User = require('../models/user');

exports.signup = (req, res, next) => {

    if (req.body.password && req.body.email && req.body.password != req.body.email) {
        if (/[a-z]/.test(req.body.password) && /[A-Z]/.test(req.body.password) && /[0-9]/.test(req.body.password) && /[^\w]/.test(req.body.password) && req.body.password.length > 7) {
            argon2.hash(req.body.password)
            .then(hash => {
                const user = new User({
                    email: req.body.email,
                    password: hash
                });
                user.save()
                .then(() => {
                    res.status(201).json({ message: 'Welcome to Piquante !!!' });
                })
                .catch(error => res.status(400).json({ error: error }));
            })
            .catch(error => res.status(500).json({ error: error }));
        } else {
            throw new Error('Weak password !');
        }
    } else {
        throw new Error('The pass shall be different of the email !');
    }
};

exports.login = (req, res, next) => {
    User.findOne({ email: req.body.email })
    .then(user => {
        if (!user) {
            return res.status(401).json({ error: 'User not found..' });
        }

        argon2.verify(user.password, req.body.password)
        .then(valid => {
            if (!valid) {
                return res.status(401).json({ error: 'Authentication failed..' });
            }
            res.status(200).json({
                userId: user._id,
                token: jwt.sign(
                    { userId: user._id },
                    'fesf758es75f4es32f4esf6874es6fes',
                    { expiresIn: '12h' }
                )
            });
        })
        .catch(error => res.status(400).json({ error }));
    })
    .catch(error => res.status(500).json({ error }));
  };