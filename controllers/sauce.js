const fs = require('fs');

const Sauce = require('../models/sauce');

exports.findAll = (req, res, next) => {
    Sauce.find()
    .then(sauces => res.status(200).json(sauces))
    .catch(error => res.status(400).json({ error }));
};

exports.createSauce = (req, res, next) => {
    const sauceObject = JSON.parse(req.body.sauce);

    const sauce = new Sauce({
      ...sauceObject,
      likes: 0,
      dislikes: 0,
      usersLiked: [],
      usersDisliked: [],
      imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
    });
    sauce.save()
    .then(() => res.status(201).json({ message: 'Sauce created !' }))
    .catch(error => res.status(400).json({ error: error }));
};

exports.findOne = (req, res, next) => {
    Sauce.findOne({ _id: req.params.id })
    .then(sauce => res.status(200).json(sauce))
    .catch(error => res.status(404).json({ error }));
};

exports.updateSauce = (req, res, next) => {
    Sauce.findOne({ _id: req.params.id })
    .then(sauce => {
        fs.unlink(`images/${sauce.imageUrl.split('/images/')[1]}`, () => {
            console.log(`Delete image : ${sauce.imageUrl.split('/images/')[1]}`);
        });
    })
    .catch(error => res.status(404).json({ error }));

    const sauceObject = req.file ?
    {
    ...JSON.parse(req.body.sauce),
    imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
    } : { ...req.body };

    Sauce.updateOne({ _id: req.params.id }, { ...sauceObject, _id: req.params.id })
    .then(() => res.status(200).json({ message: 'Sauce modifiée !'}))
    .catch(error => res.status(400).json({ error }));
};

exports.deleteSauce = (req, res, next) => {
    Sauce.findOne({ _id: req.params.id })
      .then(sauce => {
        const filename = sauce.imageUrl.split('/images/')[1];
        fs.unlink(`images/${filename}`, () => {
            sauce.deleteOne({ _id: req.params.id })
            .then(() => res.status(200).json({ message: 'Sauce supprimée !'}))
            .catch(error => res.status(400).json({ error }));
        });
      })
      .catch(error => res.status(500).json({ error }));
};




exports.likeSauce = (req, res, next) => {
    Sauce.findOne({ _id: req.params.id})
    .then(sauce => {
        
        if (!sauce) {
            res.status(404).json({message: 'Sauce non trouvée..'});
        }
        
        if ((req.body.like || req.body.like === 0) && req.body.userId) {
            switch (req.body.like) {
                
            case 1:
                if (sauce.usersLiked && sauce.usersLiked.includes(req.body.userId)) {

                    res.status(200).json({ message: 'Sauce déjà likée !'});
                    return;
                } else {
                    sauce.likes++;
                    if (sauce.usersLiked) {
                        sauce.usersLiked.push(req.body.userId);
                    } else {
                        sauce.usersLiked = [req.body.userId];
                    }
                }
                break;

            case -1:
                if (sauce.usersDisliked && sauce.usersDisliked.includes(req.body.userId)) {

                    res.status(200).json({ message: 'Sauce déjà dislikée !'});
                    return;
                } else {
                    sauce.dislikes++;
                    if (sauce.usersDisliked) {
                        sauce.usersDisliked.push(req.body.userId);
                    } else {
                        sauce.usersDisliked = [req.body.userId];
                    }
                }
                break;

            case 0:
                if (removeElementFromArray(sauce.usersLiked, req.body.userId) !== false) {
                    sauce.usersLiked = removeElementFromArray(sauce.usersLiked, req.body.userId);
                    sauce.likes--;
                }
                if (removeElementFromArray(sauce.usersDisliked, req.body.userId) !== false) {
                    sauce.usersDisliked = removeElementFromArray(sauce.usersDisliked, req.body.userId);
                    sauce.dislikes--;
                }
                break;
            default:
                res.status(400).json({ message: 'Mauvaise requête de like'})
                return;
            }
            
            Sauce.updateOne({ _id: req.params.id }, sauce)
            .then((update) => {
                if (update && update.nModified === 1) {
                    res.status(200).json({ message: 'Sauce modifiée !'})
                } else {
                    res.status(400).json({ message: 'Aucune modification apportée..'})
                }
            })
            .catch(error => res.status(400).json({ error }));
        } else {
            res.status(400).json({ message: 'Requête incorrecte..'})
        }
    })
    .catch(error => res.status(404).json({ error }));
};


function removeElementFromArray(iArray, iElement) {

    if (iArray && typeof iArray === 'object' && iElement) {

        if (iArray.includes(iElement)) {
            return iArray.filter(element => {
                if (element != iElement) {
                    return element;
                }
            });
        }
    }
    return false;
}

