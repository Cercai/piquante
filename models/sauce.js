
const mongoose = require('mongoose');

const sauceSchema = mongoose.Schema({
    // identifiant unique MongoDB pour l'utilisateur qui a créé la sauce
    userId: { type: String, required: true },
    // nom de la sauce
    name: { type: String, required: true},
    // fabriquant de la sauce
    manufacturer: { type: String, required: true },
    // description de la sauce
    description: { type: String, required: true },
    // principal ingrédient
    mainPepper: { type: String, required: true },
    // url de l'image de la sauce
    imageUrl: { type: String, required: true },
    // nombre entre 1 et 10 décrivant la sauce
    heat: { type: Number, required: true },
    //  nombre d'utilisateurs qui aiment la sauce
    likes: { type: Number, required: true },
    //  nombre d'utilisateurs qui n'aiment pas la sauce
    dislikes: { type: Number, required: true },
    // tableau d'identifiants d'utilisateurs ayant aimé la sauce
    usersLiked: { type: [String], required: true },
    // tableau d'identifiants d'utilisateurs n'ayant pas aimé la sauce.
    usersDisliked: { type: [String], required: true },
});

module.exports = mongoose.model('Sauce', sauceSchema);
