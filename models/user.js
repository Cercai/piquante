
const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const userSchema = mongoose.Schema({
    // adresse électronique de l'utilisateur
    email: { type: String, required: true, unique: true },
    // hachage du mot de passe de l'utilisateur.
    password: { type: String, required: true }
});

// plugin Mongoose pour assurer le caractère unique
userSchema.plugin(uniqueValidator);

module.exports = mongoose.model('User', userSchema);
 